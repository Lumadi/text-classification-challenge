# ZINDI Competitions 

### Sustainable Development Goals (SDGs): Text Classification Challenge:

The aim of this competition is to classify text content by its relevance to the measurable indicators of the United Nations’ Sustainable Development Goal #3 – Health and Well-Being

### The Data 

SDG 3 has 14 targets and 27 indicators.The training data for this challenge includes approximately 3,000 web-scraped text from tenders, programs, and documents, as well as news articles about international development and humanitarian aid, and finally text descriptions of organizations involved in those sectors.